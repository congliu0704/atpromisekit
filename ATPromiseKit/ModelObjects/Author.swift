//
//  Author.swift
//  ATPromiseKit
//
//  Created by Dejan on 01/10/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

protocol Author {
    var authorID: String { get }
    var firstName: String? { get }
    var lastName: String? { get }
    var fullName: String? { get }
}
